const image = document.querySelector("#image");
const fileElem = document.querySelector("#file");
const converter = { "pbm" : convertPBM2img, "ppm" : convertPPM2img, "pgm" : convertPGM2img};
const dropArea = document.querySelector('#drop-area');
const popup = document.querySelector('#popup');
/** 
 * Afficher un tableau de pixel
 * param img (Array) : Un tableau de lignes de pixel où un pixel est défini en rgb (String)
 * 
 * **/
function display(img){
  image.innerHTML = "";
  for (let ligne of img){
	  let l = document.createElement("tr");
	  l.classList.add("line");
	  for (let pixel of ligne){
		  let p = document.createElement("td");
		  p.classList.add("pixel");
		  p.style.backgroundColor = pixel;
		  l.appendChild(p);
	  }
	image.appendChild(l);
  }
};

function convertPBM2img(fichier){
	let lines = fichier.split('\n');
	let result = [];

	if (lines[0] != "P1"){
		alert("Format de fichier non conforme au PBM");
	} else {
		for (let i = 2; i < lines.length; i++){
			let line = lines[i].trim().replaceAll(" ", "");
			if (line.length > 0){
				let l = new Array();
				for (let colonne = 0; colonne < line.length; colonne ++){
					l.push((line[colonne] == "0")?"white":"black");
				}
				result.push(l);
			}
		}
	}
	return result;
}

function convertPGM2img(fichier){
	let lines = fichier.split('\n');
	let result = [];

	if (lines[0] != "P2"){
		alert("Format de fichier non conforme au PGM");
	} else {
		const maxval = parseInt(lines[2], 10);
 
		for (let i = 3; i < lines.length; i++){
			let line = lines[i].trim();
			if (line.length > 0){
				line = line.split(" ");
				let l = new Array();
				for (let colonne = 0; colonne < line.length; colonne ++){
					let value = Math.trunc(parseInt(line[colonne], 10) * 255 / maxval);
					l.push("rgb(" + value + ", " + value + ", " + value + ")");
				}
				result.push(l);
			}
		}
	}
	return result;
}

function convertPPM2img(fichier){
	let lines = fichier.split('\n');
	let result = [];

	if (lines[0] != "P3"){
		alert("Format de fichier non conforme au PPM");
	} else {
		for (let i = 3; i < lines.length; i++){
			let line = lines[i].trim();
			if (line.length > 0){
				line = line.split(" ");
				let l = new Array();
				for (let colonne = 0; colonne < line.length; colonne += 3){
					l.push("rgb(" + line[colonne] + ", " + line[colonne + 1] + ", " + line[colonne + 2] + ")");
				}
				result.push(l);
			}
		}
	}
	return result;
}

fileElem.addEventListener('change',function(){
	handleFile(this.files[0]);
 });

 

 ['dragenter', 'dragover', 'dragleave', 'drop'].forEach(eventName => {
	dropArea.addEventListener(eventName, preventDefaults, false);
  });
  
  function preventDefaults (e) {
	e.preventDefault();
	e.stopPropagation();
  }
  
  ['dragenter', 'dragover'].forEach(eventName => {
	dropArea.addEventListener(eventName, highlight, false);
  });
  
  ['dragleave', 'drop'].forEach(eventName => {
	dropArea.addEventListener(eventName, unhighlight, false);
  });
  
  function highlight(e) {
	dropArea.classList.add('highlight');
  }
  
  function unhighlight(e) {
	dropArea.classList.remove('highlight');
  }
  
dropArea.addEventListener('drop', handleDrop, false)

function handleFile(file){
	let fileReader = new FileReader();

	fileReader.onload = function(){
		const extension = file.name.split('.').pop().toLowerCase();
		if (converter[extension] === undefined){
			alert("Format de fichier non autorisé. Seuls les formats .pbm, .pgm ou .ppm sont autorisés.");
		} else {
			const fichier = fileReader.result.replaceAll('\r', '');
			const img = converter[extension](fichier);
			
			display(img);
		}
	}

  	fileReader.readAsText(file);
}

function handleDrop(e) {
	let dt = e.dataTransfer;
	let file = dt.files[0];
	handleFile(file);
}


document.querySelector('#close').addEventListener('click', function(){
    popup.style.display = "none";
});

document.querySelector('#pop').addEventListener('click', function(){
	popup.style.display = 'flex';
});
